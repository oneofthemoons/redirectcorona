import os
from flask import Flask,redirect, request

app = Flask(__name__)

@app.route('/')
def hello():
    uri = ""
    user_hash = ""
    from_route = ""
    print(request.args)
    for e in request.args.keys():
        if e == "uri":
            uri = request.args[e]
        elif e == "user_hash":
            user_hash = request.args[e]
        elif e == "from_route":
            from_route = request.args[e]

    #@TODO DB instead of print
    print(uri)
    print(user_hash)
    print(from_route)
    return redirect(uri, code=302)


if __name__ == '__main__':
    # Bind to PORT if defined, otherwise default to 5000.
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)